package com.example.springbootthymeleaf.controller;

import com.example.springbootthymeleaf.annotation.ALog;
import com.example.springbootthymeleaf.service.CkglService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Api(tags = "出库管理")
@RestController
public class CkglController extends BaseController {
    @Autowired
    CkglService ckglService;

    @ALog(desc = "查询出库信息")
    @GetMapping("/selecck")
    @ApiOperation(value = "查询所有数据")
    public List<Map<String, Object>> selecck() {
        return ckglService.selecck();
    }

    @ALog(desc = "修改出库信息")
    @PutMapping("/updateckxx")
    @ApiOperation(value = "修改出库信息")
    public Boolean UpdateByCkId(HttpServletRequest request) {
        Map<String, Object> params = getParams(request);
        Boolean aBoolean = ckglService.UpdateById(params);
        return aBoolean;
    }

    @ALog(desc = "新增出库信息")
    @PutMapping("/addckxx")
    @ApiOperation(value = "新增出库信息")
    public Boolean addCkxx(HttpServletRequest request) {
        Map<String, Object> params = getParams(request);
        Boolean aBoolean = ckglService.addCkxx(params);
        return aBoolean;
    }

    @ALog(desc = "删除出库信息")
    @DeleteMapping("/delckxx")
    @ApiOperation(value = "删除出库信息")
    public Boolean delCkxx(HttpServletRequest request) {
        Map<String, Object> params = getParams(request);
        Boolean aBoolean = ckglService.delCkxx(params);
        return aBoolean;
    }

    @ALog(desc = "提交出库信息")
    @PostMapping("/updateck")
    @ApiOperation(value = "提交出库信息")
    public Boolean updateCk(HttpServletRequest request) {
        Map<String, Object> params = getParams(request);
        Boolean aBoolean = ckglService.updateCk(params);
        return aBoolean;
    }


}
