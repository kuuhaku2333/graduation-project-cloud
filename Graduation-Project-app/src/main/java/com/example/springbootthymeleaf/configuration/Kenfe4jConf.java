package com.example.springbootthymeleaf.configuration;/**
 * Created by penglee on 2019/7/26.
 */

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;


/**
 * Swagger2
 * <p>
 * Description: <br>
 * Created in 2019/7/26 10:09 <br>
 *
 * @author FN
 */
@Configuration
@EnableSwagger2WebMvc
public class Kenfe4jConf {

	@Bean
	@Order(value = 1)
	public Docket groupRestApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(groupApiInfo())
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.example.springbootthymeleaf.controller"))
				.paths(PathSelectors.any())
				.build();
	}

	private ApiInfo groupApiInfo(){
		return new ApiInfoBuilder()
				.title("swagger-bootstrap-ui很棒~~~！！！")
				.description("<div style='font-size:14px;color:red;'>swagger-bootstrap-ui-demo RESTful APIs</div>")
				.termsOfServiceUrl("http://www.group.com/")
				.contact(new Contact("测试", "", "group@qq.com"))
				.version("1.0")
				.build();
	}

//	/**
//	 * 创建RestApi 并包扫描controller
//	 * @return
//	 */
//	@Bean
//	public Docket createRestApi() {
//		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo())
//				.select()
//				.apis(RequestHandlerSelectors.any())// 对所有api进行监控
//				.paths(PathSelectors.any())
//				//不显示错误的接口地址
//				.paths(Predicates.not(PathSelectors.regex("/error.*")))//错误路径不监控
//				.build()
//				.groupName("测试毕设")
//				.select()
//				.build();
//	}
//
//	/**
//	 * 创建Swagger页面 信息
//	 * @return
//	 */
//	private ApiInfo apiInfo() {
//		return new ApiInfoBuilder().title("用户微服")
//				.description("用户微服")
//				.version("1.0.0-SNAPSHOT")
//				.build();
//	}
}

//	@Bean
//	public Docket docket20101000() {
//		String menuKey = "20101000";
//		return getDocketByKey(menuKey);
//	}
//
//	@Bean
//	public Docket docket10301000() {
//		// 证照管理-系统配置-组织机构
//		String menuKey = "10301000";
//		return getDocketByKey(menuKey);
//	}
//
//	@Bean
//	public Docket docket10302000() {
//		// 证照管理-系统配置-角色管理
//		String menuKey = "10302000";
//		return getDocketByKey(menuKey);
//	}
//
//	@Bean
//	public Docket docket10303000() {
//		// 证照管理-系统配置-数据字典
//		String menuKey = "10303000";
//		return getDocketByKey(menuKey);
//	}
//
//	@Bean
//	public Docket docket10304000() {
//		// 证照管理-系统配置-印章管理
//		String menuKey = "10304000";
//		return getDocketByKey(menuKey);
//	}
//
//	@Bean
//	public Docket docket10105000() {
//		// 权限配置
//		String menuKey = "10105000";
//		return getDocketByKey(menuKey);
//	}
//
//	@Bean
//	public Docket docket10104000() {
//		// 定义机构
//		String menuKey = "10104000";
//		return getDocketByKey(menuKey);
//	}
//
//	@Bean
//	public Docket docket10103000() {
//		// 类型维护
//		String menuKey = "10103000";
//		return getDocketByKey(menuKey);
//	}
//
//	@Bean
//	public Docket docket10102000() {
//		// 表单配置
//		String menuKey = "10102000";
//		return getDocketByKey(menuKey);
//	}
//
//	@Bean
//	public Docket docket10101000() {
//		// 电子证照
//		String menuKey = "10101000";
//		return getDocketByKey(menuKey);
//	}
//
//	@Bean
//	public Docket docketOther() {
//		return getDocket(input -> {
//			Predicate<RequestHandler> predicate1 = RequestHandlerSelectors.withMethodAnnotation(MenuCheck.class);
//			Predicate<RequestHandler> predicate2 = RequestHandlerSelectors.withClassAnnotation(MenuCheck.class);
//			return !(predicate1.apply(input) || predicate2.apply(input));
//		}, "other");
//	}
//
//	private Docket getDocketByKey(String menuKey) {
//		return getDocket(handler -> {
//			List<String> menuCheck = MenuCheckHelper.getMenuCheckByRequest(handler);
//			return menuCheck != null && menuCheck.size() > 0 && menuCheck.contains(menuKey);
//		}, menuKey);
//	}
//
//	private Docket getDocket(Predicate<RequestHandler> selector, String groupName) {
//		return new Docket(DocumentationType.SWAGGER_2)
//				.enable(!ENV_PROD.equals(active))
//				.apiInfo(apiInfo())
//				.globalOperationParameters(getHeader())
//				.select()
//				//添加包扫描
//				.apis(selector)
//				.paths(Predicates.not(PathSelectors.regex("/error.*")))
//				.paths(Predicates.not(PathSelectors.regex("/actuator.*")))
//				.paths(PathSelectors.any())
//				.build()
//				.groupName(groupName);
//	}
//
//	private List<Parameter> getHeader() {
//		List<Parameter> parameters = new ArrayList<>();
//		parameters.add(new ParameterBuilder()
//				.name("Authorization")
//				.description("认证token")
//				.modelRef(new ModelRef("string"))
//				.parameterType("header")
//				.required(false)
//				.build());
//		return parameters;
//	}
//
//
//	private ApiInfo apiInfo() {
//		return new ApiInfoBuilder()
//				.title("三门峡崤云安全-电子材料管理平台 RESTful APIs")
//				.description("电子材料管理平台")
//				.termsOfServiceUrl("")
//				.contact(new Contact("三门峡崤云安全", "", "dev@smxxyaq.com"))
//				.version(version)
//				.build();
//	}
//
	
